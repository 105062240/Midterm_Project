# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Karta's Chatroom
* Key functions (add/delete)
    1. chat
    2. load message history
    3. chat with new user 
    4. sign in/up
* Other functions (add/delete)
    1. background music
    2. change name
    3. secret chatroom
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
* 起始畫面
    * <img src = "public/re/1.jpg">
左邊的按鈕是我的IG,右邊是FB,中間是進入登入頁面

且此時有背景音樂 歌名是Setember
* 登入頁面
    * <img src = "public/re/2.jpg">
有SIGN IN,SIGN UP及GOOGLE和FB登入

帳密打錯時會有alert說打錯了
* <img src = "public/re/3.jpg">
按forgot會叫你再想一下
* <img src = "public/re/4.jpg">
不管用甚麼方式登入 都會跳轉到聊天室介面
* <img src = "public/re/6.jpg">
左上角的名字會依照當前USER而改變

而傳出的訊息包含USER的名字,相片及內容
* <img src = "public/re/8.jpg">
將游標移到對話內容上則可得知發出訊息的時間
* <img src = "public/re/15.jpg">
點一下右上角的MENU 他會很可愛的展開
* <img src = "public/re/10.jpg">
其中Setting可以改名字,profile可以連到溫妮的IG,Log out就是登出
* <img src = "public/re/11.jpg">

而聊天功能除了一般對話外,輸入Create/加A開頭的房名 ex.A777可創一間加密房間並設定密碼
* <img src = "public/re/12.jpg">
輸入*加上加密房間的房名即可輸入密碼進入
* <img src = "public/re/13.jpg">
輸入@或+加上房名則可創一般房間
* <img src = "public/re/14.jpg">
最後,輸入cd..則可回到最初始的大房間

另外此聊天室還有通知功能
* <img src = "public/re/9.jpg">
以上是我的聊天室
## Security Report (Optional)
