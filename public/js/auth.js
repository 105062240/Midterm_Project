// Login with Email/Password
function initApp() {
var txtEmail = document.getElementById('txtEmail');
var txtPassword = document.getElementById('txtPassword');
var btnLogin = document.getElementById('btnLogin');
var btnSignUp = document.getElementById('btnSignUp');
var btnLogout = document.getElementById('btnLogout');
var forget = document.getElementById('forget');
btnLogin.addEventListener('click', function() {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
        window.location.replace('chat.html');
    }).catch(function (error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    window.alert("錯ㄖ");
    //window.location.replace('auth.html');   
    
    });
});
forget.addEventListener('click', function () {
    window.alert("想一下每幹");
});

btnSignUp.addEventListener('click', function () {        
    
     var email = txtEmail.value;
    var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            window.location.replace('chat.html');
        }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/weak-password') {
         create_alert('error','nono')
        } else {
            alert(errorMessage);
         }
         console.log(error);
         });
});

// Login with Google


btnLoginGooglePop.addEventListener('click', function () {
    var btnLoginGooglePop = document.getElementById('btnLoginGooglePop');
    var btnLoginGoogleRedi = document.getElementById('btnLoginGoogleRedi');
    var provider = new firebase.auth.GoogleAuthProvider();
    console.log('signInWithPopup');
    firebase.auth().signInWithPopup(provider).then(function (result) {
        var token = result.credential.accessToken;
        var user = result.user;
        window.location.replace('chat.html');
    }).catch(function (error) {
        console.log('error: ' + error.message);
    });
});



firebase.auth().getRedirectResult().then(function (result) {
    if (result.credential)
        var token = result.credential.accessToken;
    var user = result.user;
}).catch(function (error) {
    console.log('error: ' + error.message);
});

// Login with Facebook


btnLoginFBPop.addEventListener('click', function() {
    var btnLoginFBPop = document.getElementById('btnLoginFBPop');
    var btnLoginFBRedi = document.getElementById('btnLoginFBRedi');

    var facebook_provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(facebook_provider).then(function (result) {
        console.log('signInWithPopup');
        var token = result.credential.accessToken;
        window.location.replace('chat.html');
        var user = result.user;
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
    });
});



// Manage Users
//var usrDiv = document.getElementById('usrDiv');
//var usrName = document.getElementById('usrName');
//var useEmail = document.getElementById('useEmail');

//firebase.auth().onAuthStateChanged(user => {
//    if (user) {
//        console.log(user);
//        usrDiv.style.visibility = 'visible';
//        usrName.innerHTML = 'User Name: ' + user.displayName;
//        useEmail.innerHTML = 'User Email: ' + user.email;
//    } else {
//        console.log('not logged in');
//  /      usrDiv.style.visibility = 'hidden';
//    }
//});
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
}
window.onload = function () {
    initApp();
};